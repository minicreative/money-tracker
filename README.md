# money-tracker

### Setup

Do `npm install`  

Set the following environment variables:
```
mt_port: port which Express server listens on
mt_secret: secret used for authentication
mt_mongo_host: MongoDB host address
mt_mongo_name: MongoDB database name
mt_mongo_user: MongoDB username
mt_mongo_pass: MongoDB password
```

### Run
`npm run dev` to watch serve and watch UI and API in paralell  
`npm run start` to build application and start server demon